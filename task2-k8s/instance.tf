resource "google_compute_instance" "test_instance" {
  name = var.instance_name
  machine_type = var.instance_machine_type
  zone         = var.instance_zone

  boot_disk {
      initialize_params {
          image = var.instance_image
          type    = var.instance_disk_type
      }
  }

  network_interface {
      network = "default"
      access_config {
          
      }
  }

  metadata = {
      name = var.instance_name
      ssh-keys = "${var.user}:${file(var.ssh_key_path)}"
  }

  metadata_startup_script = file(var.path_startup_script)

  tags = ["http-server"]
}

resource "google_compute_firewall" "http-server" {
  name = "default-allow-http-to-test"
  network = "default"
  allow {
      protocol = var.protocol_type
      ports = var.allow_ports
  }

  source_ranges = var.ranges
  target_tags = ["http-server"]
}

