package com.demo.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import com.demo.entity.Product;

@Service
public class ProductService 
{
    public static List<Product> getAll()
    {
        Product product1 = new Product(1, "Phone", 10.32);
        Product product2 = new Product(2, "TV", 1000.50);
        List<Product> products = new ArrayList<>();
        products.add(product1);
        products.add(product2);
        return products;
    }
}