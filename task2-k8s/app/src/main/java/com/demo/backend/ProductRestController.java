package com.demo.backend;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.demo.service.ProductService;
import com.demo.entity.Product;
import java.util.List;


@RestController
@RequestMapping(value="api/v1/")
public class ProductRestController 
{

    @GetMapping(value="/products")
    public List<Product> getAll()
    {
        return ProductService.getAll();
    }
}