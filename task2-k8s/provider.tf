provider "google" {
  credentials = file("${var.key_id}.json")
  project = var.project_id
  region = var.region
}