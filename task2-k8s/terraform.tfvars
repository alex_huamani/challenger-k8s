key_id = var.key_id
region = var.region
project_id = var.project_id
user = var.user

instance_name = "jenkins-vm"
instance_zone = var.instance_zone
instance_machine_type = "e2-small"
instance_image = "centos-7"
instance_disk_type = "pd-standard"
path_startup_script = "startup_script.txt"
ssh_key_path = "ssh/id_rsa.pub"

allow_ports = ["80", "22", "8080"]
protocol_type = "tcp"
ranges = ["0.0.0.0/0"]