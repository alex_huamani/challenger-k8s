# TASK 2

El challenger se desarrollo en GNU/Linux Mint. Para el desarrollo de esta tarea se requiere la instalación y configuración de las siguientes herramientas:

* Una *cuenta de Google Free* como mínimo. Así como, descargar su credencial json, que se explicara luego donde insertanlo. Por último tener su project_id que lo encontrara en su json file.

* Terraform
* Kubernetes y kubectl
* Docker
* Git
* Python
* Ansible
* Putty-tool
```bash
    sudo apt-get install putty-tools
```
* Java

## Configuraciones previas al despliegue

1. Descargar el repositorio, si lo tiene descargado obvie el paso, donde estará los proyectos con el comando:
```bash
    git clone git@bitbucket.org:miguel_huamani_aldude/challenger-k8s.git
```
2. Copiar dentro de la carpeta task2-k8s tu credencial de google (json file).
3. Abrir el Makefile, con el editor de texto de su preferencia, y rellenar las siguientes variables: 
    * KEY_ID: el nombre de su credential
    * REGION: la región donde creara el ambiente
    * PROJECT_ID: es el proyecto que se genera cuando inicia por primera vez. Lo encontraras también de tu json file.

    Opcionalmente tendrá dos parámetros:
    * HOST: Es el nombre del host de su VM, el que ud guste y que estar asociado con el de su inventory en Ansible.
    * USER: el nombre para su user que también debe ser el mismo en la configuración de su inventory.

4. Deberá crear un archivo pem, así como un id_rsa.pub que los podrá guardar dentro de la carpeta ssh. Una opción para generarlo es con los siguientes pasos, de preferencia dentro de la carpeta ssh.
```bash
    puttygen -t rsa -b 2048 -C "user@host" -o keyfile.ppk
```
El te generara un keyfile.ppk, el nombre lo puede cambiar.

```bash
    puttygen -L keyfile.ppk
```
Te generara un key público del keyfile.ppk generado anteriormente. Ese texto que saldrá en el terminal cópielo y pégelo dentro de un archivo llamado id_rsa.pub y guárdelo dentro de la carpeta ssh.
```bash
    puttygen keyfile.ppk -O private-openssh -o server.pem

    chmod 400 server.pem
```
Por último estos dos comandos, el primero es para generar el pem esperado y el segundo para darle unos permisos. Estos archivos almacenarlos dentro de la carpeta "ssh"(task2-k8s/ssh) (si no esta creado, generenlo). Con estos pasos ya podríamos pasar a la siguiente etapa. Estos datos deberan agregarse en el archivo ansible/hosts.txt, reemplazando la ruta absoluta del pem por [PATH_FIJO] y el nombre del pem por [PEM_NAME].

5. Si deseas cambiar algunas características para el infraestructura, lo puedes hacer dentro del terraform.tfvar.

## Despliegue de la aplicación

Mencionar que la automatización sera bajo el archivo Makefile, que mediante unos comandos podrá construir y destruir el ambiente, así como el despliegue de la aplicación.

Para la creación del clúster de Kubernetes, así como el despliegue de la aplicación, ud deberá ejecutar el comando:
```bash
    make build-now action=CREATE init=true
```
el parámetro init=true solo agregara siempre en cuando sea la primera vez que construye, las siguientes veces, no sera necesario.

Para destruir todo el ambiente solo necesitara ejecutar:
```bash
    make build-now action=DESTROY
```
Se da cuenta que no necesita init=true, y solo cambia el valor del parámetro action.

Para ver salida de datos como la ip que siempre vera al finalizar la ejecución de una construcción, solo ejecutara:
```bash
    make build-now action=OUTPUT
```

Por último si ud solo desea hacer init al proyecto, puede ejecutar el comando:
```bash
    make prepare init=true
 ```

 Aquí si es necesario utilizar el parámetro init=true sino no obtendrá resultados.

 ```bash
    make build-vm action=CREATE init=true
 ```
 Es mus similar al primer comando, pero este permite crear un VM para tu aplicación Jenkins, el init=true, tiene la misma consideración. Aquí hay algo importante que mencionar, cuando ud haya terminado de crear su VM (Virtual Machine) con el SO Centos, será necesario copiar la ip que le devuelve y copiarla dentro del inventory file llamado ansible/hosts.txt, reemplazarlo por la ip 0.0.0.0 que encontrará,esto le servirá para ejecutar el comando make build-jenkins y poder crear su aplicación Jenkins.

```bash
    make build-vm action=DESTROY
 ``` 
 Para borrar todo el ambiente y la aplicación.

 ```bash
    make build-vm action=OUTPUT
 ```
 Para ver parametros de salida.
```bash
    make build-jenkins
 ```
Este comando te creara la herramienta Jenkins, así como un job, la instalación del plugin pipeline y un share lib

Nota: Como lo mencione al inicio todo fue desarrollado dentro del SO GNU/Linux Mint, por lo tanto, se recomienda que los pruebe dentro de uns distribución que sea de Debiab, como puede ser Ubuntu, de los contrario los resultados podrías ser diferentes, sobre todo tomar en cuenta las herramientas necesarias o algún inconveniente no deseado.

De esta manera hemos cubierto todo con lo que respecta al task2 del Challenger. Gracias