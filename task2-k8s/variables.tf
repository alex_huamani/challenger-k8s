variable "key_id" {
    type = string
}

variable "region" {
    type = string
}

variable "project_id" {
    type = string
}

variable "instance_name" {
  type = string
}

variable "instance_zone" {
    type = string
}

variable "instance_machine_type" {
    type = string
}

variable "instance_image" {
    type = string
}

variable "instance_disk_type" {
    type = string
}

variable "path_startup_script" {
    type = string
}

variable "ssh_key_path" {
    type = string
}

variable "allow_ports" {
    type = list
}

variable "protocol_type" {
    type = string
}

variable "ranges" {
    type = list
}

variable "user" {
    type = string
}