resource "kubernetes_deployment" "app" {
  metadata {
    name = var.app_name
    labels = {
      app = var.app_name
    }
  }

  spec {
    replicas = 2

    selector {
      match_labels = {
        app = var.app_name
      }
    }

    template {
      metadata {
        labels = {
          app = var.app_name
        }
      }

      spec {
        container {
          image = var.app_image_name
          name  = var.app_name

          port {
            container_port = 80
            protocol = "TCP"
          }

          # resources{
          #   limits{
          #     cpu    = "0.5"
          #     memory = "512Mi"
          #   }
          #   requests{
          #     cpu    = "250m"
          #     memory = "50Mi"
          #   }
          # }
        }
      }
    }
  }
}