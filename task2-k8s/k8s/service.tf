resource "kubernetes_service" "app_service" {
  metadata {
    name = "${var.app_name}-service"
  }
  spec {
    selector = {
      app = var.app_name
    }
    session_affinity = "ClientIP"
    port {
      port = 80
      target_port = 80
    }

    type = "LoadBalancer"
  }
}