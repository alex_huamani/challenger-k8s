variable "key_id" {
    type = string
}

variable "region" {
    type = string
}

variable "project_id" {
    type = string
}

variable "cluster_name" {
    type = string
}

variable "cluster_region" {
    type = string
}

variable "cluster_node_count" {
    type = number
}

variable "node_pool_region" {
    type = string
}

variable "node_machine_type" {
    type = string
}

variable "node_count" {
    type = number
}

variable "app_name" {
    type = string
}

variable "app_image_name" {
    type = string
}