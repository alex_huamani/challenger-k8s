resource "google_container_cluster" "cluster" {
  name = "${var.cluster_name}-gke-cluster"
  location = var.cluster_region
  remove_default_node_pool = true
  initial_node_count = var.cluster_node_count
  master_auth {
      username = ""
      password = ""

      client_certificate_config {
          issue_client_certificate = false
      }
  }
}

resource "google_container_node_pool" "name" {
  name = "node-pool"
  location = var.node_pool_region
  cluster = google_container_cluster.cluster.name
  node_count = var.node_count
  node_config {
      preemptible = true
      machine_type = var.node_machine_type
      metadata = {
          disable-legacy-endpoints = "true"
      }
      oauth_scopes = [
        "https://www.googleapis.com/auth/logging.write",
        "https://www.googleapis.com/auth/monitoring",
        ]
  }
}
