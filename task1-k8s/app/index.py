from flask import Flask, json, request
import socket

app = Flask(__name__)
@app.route("/")
def index():
    return json.dumps({'message': "Hello World to my app"})

@app.route("/greetings")
def greetings():    
    hostname = socket.gethostname()
    return json.dumps({'message': "Hello World from {0}".format(hostname)})

@app.route("/square") 
def square():
    number = request.args.get('x')
    resp = int(number) * int(number)
    message = 'number: {0}, square: {1}, donde {2} es el cuadrado de {3}. El cuadrado de {4} es {5}.' 
    message = message.format(number, resp, resp, number, number, resp)
    return json.dumps({'message': message}), 201

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int("80"), debug=False)