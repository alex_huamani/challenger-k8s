key_id = var.key_id
region = var.region
project_id = var.project_id

cluster_region = "us-central1-a"
cluster_name = "task1"
cluster_node_count = 1

node_pool_region = "us-central1-a"
node_machine_type = "n1-standard-1"
node_count = 1

app_name = "python-app"
app_image_name = "gcr.io/extended-byway-277803/my-python-app:latest"