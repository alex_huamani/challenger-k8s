# TASK 1

El challenger se desarrollo en GNU/Linux Mint. Para el desarrollo de esta tarea se requiere la instalación y configuración de las siguientes herramientas:

* Una *cuenta de Google Free* como mínimo. Así como, descargar su credencial json, que se explicara luego donde insertanlo. Por último tener su project_id que lo encontrara en su json file.

* Terraform
* Kubernetes y kubectl
* Docker
* Git
* Python
* Java

## Configuraciones previas al despliegue

1. Descargar el repositorio, si lo tiene descargado obvie el paso, donde estará los proyectos con el comando:
```bash
    git clone git@bitbucket.org:miguel_huamani_aldude/challenger-k8s.git
```
2. Copiar dentro de la carpeta task1-k8s tu credencial de google (json file).
3. Abrir el Makefile, con el editor de texto de su preferencia, y rellenar las siguientes variables: 
    * KEY_ID: el nombre de su credential
    * REGION: la región donde creara el ambiente
    * PROJECT_ID: es el proyecto que se genera cuando inicia por primera vez. Lo encontraras también de tu json file.

4. Deberá crear un archivo pem, así como un id_rsa.pub que los podrá guardar dentro de la carpeta ssh. Una opción para generarlo es con los siguientes pasos, de preferencia dentro de la carpeta ssh.
```bash
    puttygen -t rsa -b 2048 -C "user@host" -o keyfile.ppk
```
El te generara un keyfile.ppk, el nombre lo puede cambiar.

```bash
    puttygen -L keyfile.ppk
```
Te generara un key público del keyfile.ppk generado anteriormente. Ese texto que saldrá en el terminal cópielo y pégelo dentro de un archivo llamado id_rsa.pub y guárdelo dentro de la carpeta ssh.
```bash
    puttygen keyfile.ppk -O private-openssh -o server.pem

    chmod 400 server.pem
```
Por último estos dos comandos, el primero es para generar el pem esperado y el segundo para darle unos permisos. Estos archivos almacenarlos dentro de la carpeta "ssh" (si no esta creado, generenlo). Con estos pasos ya podríamos pasar a la siguiente etapa. Estos datos deberan agregarse en el archivo ansible/hosts.txt, reemplazando la ruta absoluta del pem por [PATH_FIJO] y el nombre del pem por [PEM_NAME].

5. Si deseas cambiar algunas características para el infraestructura, lo puedes hacer dentro del terraform.tfvar.

## Despliegue de la aplicación

Mencionar que la automatización sera bajo el archivo Makefile, que mediante unos comandos podrá construir y destruir el ambiente, así como el despliegue de la aplicación.

Para la creación del clúster de Kubernetes, así como el despliegue de la aplicación, ud deberá ejecutar el comando:
```bash
    make build-now action=CREATE init=true
```
el parámetro init=true solo agregara siempre en cuando sea la primera vez que construye, las siguientes veces, no sera necesario.

Para destruir todo el ambiente solo necesitara ejecutar:
```bash
    make build-now action=DESTROY
```
Se da cuenta que no necesita init=true, y solo cambia el valor del parámetro action.

Para ver salida de datos como la ip que siempre vera al finalizar la ejecución de una construcción, solo ejecutara:
```bash
    make build-now action=OUTPUT
```

Por último si ud solo desea hacer init al proyecto, puede ejecutar el comando:
```bash
    make prepare init=true
 ```

 Aquí si es necesario utilizar el parámetro init=true sino no obtendrá resultados.

Nota: Como lo mencione al inicio todo fue desarrollado dentro del SO GNU/Linux Mint, por lo tanto, se recomienda que los pruebe dentro de uns distribución que sea de Debiab, como puede ser Ubuntu, de los contrario los resultados podrías ser diferentes, sobre todo tomar en cuenta las herramientas necesarias o algún inconveniente no deseado.

 De esta manera hemos cubierto todo con lo que respecta al task1 del Challenger. Gracias